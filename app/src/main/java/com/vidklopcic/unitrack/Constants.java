package com.vidklopcic.unitrack;

public class Constants {
    public static String API = "https://unitrack.si/api/";
    public static String auth_header_name = "Authorization";
    public static String auth_header_value = "Token %s";
    public static Integer position_interval = 2000;
}
