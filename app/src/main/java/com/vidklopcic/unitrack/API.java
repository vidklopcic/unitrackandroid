package com.vidklopcic.unitrack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vidklopcic.unitrack.Gson.SetLocation;
import com.vidklopcic.unitrack.Gson.SetLocationResponse;
import com.vidklopcic.unitrack.Gson.SetPath;
import com.vidklopcic.unitrack.Gson.SetPathResponse;
import com.vidklopcic.unitrack.Gson.Token;
import com.vidklopcic.unitrack.Gson.User;
import com.vidklopcic.unitrack.RealmModels.Config;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;

public class API {
    public static API.FoodTrack initApi() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException
                    {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", String.format("Token %s", Config.getToken()))
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        return retrofit.create(API.FoodTrack.class);
    }

    public interface FoodTrack {
        // Request method and URL specified in the annotation
        // Callback for the parsed response is the last parameter

        @POST("set/location/")
        Call<SetLocationResponse> setLocation(@Body SetLocation location);

        @POST("path/")
        Call<SetPathResponse> setPath(@Body SetPath path);

        @GET("user/")
        Call<User> getUser();

        @GET("user/pic/")
        @Streaming
        Call<ResponseBody> getProfilePic();

        @FormUrlEncoded
        @POST("login/")
        Call<Token> login(@Field("email") String email, @Field("password") String password);
    }
}
