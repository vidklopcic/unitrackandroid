
package com.vidklopcic.unitrack.Gson;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetLocation {
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("speed")
    @Expose
    private Double speed;
    @SerializedName("heading")
    @Expose
    private Double heading;
    @SerializedName("path")
    @Expose
    private Integer path;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public Integer getPath() {
        return path;
    }

    public void setPath(Integer path) {
        this.path = path;
    }

    public SetLocation(Location location, int path) {
        setLat(location.getLatitude());
        setLng(location.getLongitude());
        setSpeed(0d);
        if (location.hasSpeed()) {
            setSpeed((double) location.getSpeed());
        }
        setHeading(0d);
        if (location.hasBearing()) {
            setHeading((double) location.getBearing());
        }

        setPath(path);
    }
}
