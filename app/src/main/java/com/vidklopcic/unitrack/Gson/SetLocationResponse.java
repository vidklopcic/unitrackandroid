
package com.vidklopcic.unitrack.Gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetLocationResponse {

    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("speed")
    @Expose
    private Double speed;
    @SerializedName("heading")
    @Expose
    private Double heading;
    @SerializedName("path")
    @Expose
    private Integer path;
    @SerializedName("age")
    @Expose
    private Integer age;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public Integer getPath() {
        return path;
    }

    public void setPath(Integer path) {
        this.path = path;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}
