package com.vidklopcic.unitrack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.vidklopcic.unitrack.Gson.Token;
import com.vidklopcic.unitrack.RealmModels.Config;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    Realm mRealm;
    API.FoodTrack mFoodTrackApi;
    EditText mEmailText;
    EditText mPasswordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailText = (EditText) findViewById(R.id.email_field);
        mPasswordText = (EditText) findViewById(R.id.password_field);
        initRealm();
        mFoodTrackApi = API.initApi();
        if (!Config.getToken().isEmpty()) {
            Intent i = new Intent(this, MainActivity.class);
            finish();
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    }


    void initRealm() {
        Realm.init(getApplicationContext());
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("e_smart_db")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(configuration);
        mRealm = Realm.getDefaultInstance();
    }

    public void tryLogin(View view) {
        mFoodTrackApi.login(mEmailText.getText().toString(), mPasswordText.getText().toString()).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) {
                    Config.setToken(response.body().getToken());
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    LoginActivity.this.finish();
                    startActivity(i);
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
            }
        });
    }
}
