package com.vidklopcic.unitrack;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import com.vidklopcic.unitrack.Gson.SetLocation;
import com.vidklopcic.unitrack.Gson.SetLocationResponse;
import com.vidklopcic.unitrack.Gson.SetPath;
import com.vidklopcic.unitrack.Gson.SetPathResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;


public class LocationService extends Service implements LocationListener {
    interface StatusCallback {
        void statusUpdate(String status);
    }

    StatusCallback mCallback;
    private static LocationService mServiceInstance = null;
    API.FoodTrack mFoodTrackApi;
    private Integer mPathId;
    Thread mLocationSender;
    Location mLastLocation;

    public LocationService() {
    }


    public void setStatusCallback(StatusCallback callback) {
        mCallback = callback;
    }

    public static boolean isRunning() {
        return mServiceInstance != null;
    }

    private final IBinder mBinder = new LocalBinder();

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public class LocalBinder extends Binder {
        LocationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocationService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mCallback = null;
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mServiceInstance = this;
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Collecting location.")
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);
        mFoodTrackApi = API.initApi();
        mFoodTrackApi.setPath(new SetPath(Double.valueOf(System.currentTimeMillis()/1000).toString())).enqueue(new Callback<SetPathResponse>() {
            @Override
            public void onResponse(Call<SetPathResponse> call, Response<SetPathResponse> response) {
                if (response.isSuccessful()) {
                    mPathId = response.body().getId();
                }
            }

            @Override
            public void onFailure(Call<SetPathResponse> call, Throwable t) {

            }
        });

        mLocationSender = new Thread(new Runnable() {
            @Override
            public void run() {
                while (isRunning()) {
                    try {
                        Thread.sleep(Constants.position_interval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (mLastLocation != null) {
                        mFoodTrackApi.setLocation(new SetLocation(mLastLocation, mPathId)).enqueue(new Callback<SetLocationResponse>() {
                            @Override
                            public void onResponse(Call<SetLocationResponse> call, Response<SetLocationResponse> response) {
                                if (response.isSuccessful()) {
                                    if (mCallback != null)
                                        mCallback.statusUpdate("sending location");
                                }
                            }

                            @Override
                            public void onFailure(Call<SetLocationResponse> call, Throwable t) {

                            }
                        });
                    }
                }
            }
        });
        mLocationSender.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mServiceInstance = null;
    }

    public void startGPS() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // necessary permission check - permission request is handled in main activity
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) return;
        manager.requestLocationUpdates(GPS_PROVIDER, 0, 0, this);
    }
}