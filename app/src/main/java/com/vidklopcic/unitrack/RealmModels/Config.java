package com.vidklopcic.unitrack.RealmModels;

import io.realm.Realm;
import io.realm.RealmObject;

public class Config extends RealmObject {
    String token = "";
    String first_name = "";
    String last_name = "";
    String email = "";

    static public Config getConfig() {
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(Config.class).count() == 0)
            createConfig();
        return realm.where(Config.class).findFirst();
    }

    static private void createConfig() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.createObject(Config.class);
        realm.commitTransaction();
    }

    static public String getToken() {
        return getConfig().token;
    }

    static public void setToken(final String token) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                getConfig().token = token;
            }
        });
    }

    static public String getFirstName() {
        return getConfig().first_name;
    }

    static public void setFirstName(final String first_name) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                getConfig().first_name = first_name;
            }
        });
    }

    static public String getLastName() {
        return getConfig().last_name;
    }

    static public void setLastName(final String last_name) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                getConfig().last_name = last_name;
            }
        });
    }

    static public String getEmail() {
        return getConfig().email;
    }

    static public void setEmail(final String email) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                getConfig().email = email;
            }
        });
    }
}